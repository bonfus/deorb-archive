tar xf AncoraDEORB3.tar --strip-components=1
tar xf AncoraDEORB4.tar --strip-components=1
tar xf AncoraDEORB5.tar --strip-components=1

set FUNC r2scanL

for test in $(find $FUNC -type f -name '*tar.gz')
    set filename $(basename -s .tar.gz $test)
    echo $test

    set element $(string split -f2 / $test)

    mkdir -p $element

    tar xzf $test -C $element
    cd $element/BulkModulus
    python ../../eos.py
    mv ../../$element.png ../../$FUNC/$element/$filename.png
    cd ..
    rm -r BulkModulus
    cd ..
    rm -r $element
end

for d in $(find $FUNC -mindepth 1 -type d)
    cd $d
    convert -annotate +20+16 '%t' BulkModulus*png -append summary.png
    echo "<html><body><img src='summary.png' /></body></html>" > index.html
    echo -e "# Summary image \n ![summary](summary.png)" > README.md
    cd -

end
